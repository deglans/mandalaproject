/*
 * Copyright (C) 2019. Deglans Dalpasso <deglans@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

plugins {
    application
    kotlin("jvm")
    id("org.openjfx.javafxplugin") version "0.0.8"
}

application {
    mainClassName = "io.gitlab.deglans.mandala.Main"
    /* DEFAULT_JVM_OPTS="--module-path=$APP_HOME/lib --add-modules=javafx.controls,javafx.fxml" */
    applicationDefaultJvmArgs = listOf("--module-path=lib", "--add-modules=javafx.controls,javafx.fxml")
}

repositories {
    mavenCentral()
}

dependencies {
    implementation(project(":core"))

    implementation(kotlin("stdlib-jdk8"))

    implementation("no.tornado:tornadofx:1.7.19")

    // 11.0.5 is not available on mvn repo TODO upgrade to 13.0.1 ?
    implementation("org.openjfx:javafx:11.0.2")
    implementation("org.openjfx:javafx-base:11.0.2")
    implementation("org.openjfx:javafx-controls:11.0.2")
}

javafx {
    version = "11.0.2"
    modules("javafx.controls", "javafx.fxml")
}
