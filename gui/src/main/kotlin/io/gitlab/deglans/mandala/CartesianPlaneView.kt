/*
 * Copyright (C) 2019. Deglans Dalpasso <deglans@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package io.gitlab.deglans.mandala

import io.gitlab.deglans.mandala.mandelbrot.SimpleMandelbrot
import io.gitlab.deglans.mandala.utils.Color
import javafx.scene.canvas.Canvas
import javafx.scene.image.WritableImage
import javafx.scene.layout.AnchorPane
import tornadofx.*
import java.awt.image.BufferedImage
import java.awt.image.RenderedImage
import java.io.File
import javax.imageio.ImageIO

class CartesianPlaneView : Fragment() {

    override val root : AnchorPane by fxml("/fxml/CartesianPlaneView.fxml")
    private val canvas : Canvas by fxid()

    init {
        drawShapes()
    }

    private fun convertColor(c: Color): javafx.scene.paint.Color {
        return javafx.scene.paint.Color.rgb(c.red, c.green, c.blue)
    }

    private fun drawShapes() {
        val simpleMandelbrot = SimpleMandelbrot()
        simpleMandelbrot.calculate()

        val writableImage = WritableImage(simpleMandelbrot.width, simpleMandelbrot.height)
        val pixelWriter = writableImage.pixelWriter

        for (w in 0 until simpleMandelbrot.width) {
            for (h in 0 until simpleMandelbrot.height) {
                pixelWriter.setColor(w, h, convertColor(simpleMandelbrot.canvas[w, h]))
            }
        }

        canvas.graphicsContext2D.drawImage(writableImage, 0.toDouble(), 0.toDouble())
    }

}
