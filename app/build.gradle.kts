/*
 * Copyright (C) 2020. Deglans Dalpasso <deglans@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

plugins {
    id("com.android.application")
    kotlin("android")
    kotlin("android.extensions")
}

android {
    sourceSets["main"].java.srcDir("src/main/kotlin")
    sourceSets["test"].java.srcDir("src/test/kotlin")
    sourceSets["androidTest"].java.srcDir("src/androidTest/kotlin")

    compileSdkVersion(29)
    defaultConfig {
        applicationId = "io.gitlab.deglans.mandala"
        minSdkVersion(23)
        targetSdkVersion(29)
        versionCode = 1
        versionName = version.toString()
        testInstrumentationRunner = "androidx.test.runner.AndroidJUnitRunner"
    }
    buildTypes {
        getByName("release") {
            isMinifyEnabled = false
            proguardFiles(getDefaultProguardFile("proguard-android.txt"), "proguard-rules.pro")
        }
        getByName("debug") {
            applicationIdSuffix = ".debug"
            isDebuggable = true
        }
    }
}

dependencies {
    implementation(fileTree(mapOf("dir" to "libs", "include" to listOf("*.jar"))))
    implementation(kotlin("stdlib-jdk8", "1.3.72"))

    releaseImplementation("io.gitlab.deglans.mandala:common-android:1.0.0-alpha1")
    debugImplementation("io.gitlab.deglans.mandala:common-android-debug:1.0.0-alpha1")

    implementation("androidx.core:core-ktx:1.1.0") // TODO: is it used?
    implementation("androidx.appcompat:appcompat:1.1.0") // TODO: is it used?
    implementation("androidx.constraintlayout:constraintlayout:1.1.3") // TODO: is it used?

    testImplementation("junit:junit:4.13")

    androidTestImplementation("androidx.test:runner:1.2.0")
    androidTestImplementation("androidx.test.espresso:espresso-core:3.2.0")
}

repositories {
    // Needed by IDEA and Gradle sync of configurations
    maven("http://localhost:8081/repository/mandala")
}

//tasks.register("clean", Delete::class) {
//    delete(rootProject.buildDir)
//}
