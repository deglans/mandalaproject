FROM ubuntu:19.10

RUN apt-get --quiet update && apt-get --quiet install -y \
  wget \
  tar \
  unzip \
  lib32stdc++6 \
  lib32z1 \
  openjdk-11-jdk

ENTRYPOINT ["bash"]
