/*
 * Copyright (C) 2020. Deglans Dalpasso <deglans@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package io.gitlab.deglans.mandala.utils

import java.awt.image.BufferedImage

actual typealias Image = BufferedImage

actual fun convertColor(c: Color): Int {
    return java.awt.Color(c.red, c.green, c.blue).rgb
}

actual fun Canvas.convert(): Image {
    val bufferedImage = BufferedImage(width, height, BufferedImage.TYPE_INT_RGB)

    for (w in 0 until width) {
        for (h in 0 until height) {
            bufferedImage.setRGB(w, h, convertColor(canvas[w][h]))
        }
    }

    return bufferedImage
}
