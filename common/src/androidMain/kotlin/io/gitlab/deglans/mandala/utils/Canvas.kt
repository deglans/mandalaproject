/*
 * Copyright (C) 2020. Deglans Dalpasso <deglans@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package io.gitlab.deglans.mandala.utils

import android.graphics.Bitmap

actual typealias Image = Bitmap

actual fun convertColor(c: Color): Int {
    return android.graphics.Color.rgb(c.red, c.green, c.blue)
}

actual fun Canvas.convert(): Image {
    val bitmap = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888)

    for (w in 0 until width) {
        for (h in 0 until height) {
            bitmap.setPixel(w, h, convertColor(canvas[w][h]))
        }
    }

    return bitmap
}
