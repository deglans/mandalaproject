/*
 * Copyright (C) 2019. Deglans Dalpasso <deglans@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package io.gitlab.deglans.mandala.utils

import kotlin.test.Test
import kotlin.test.assertEquals
import kotlin.test.assertTrue

class CartesianPlaneConverterTests {

    data class TestCase(val upLeft: Complex, val downRight: Complex, val width: Int, val height: Int)

    private val tests = arrayListOf(
        TestCase(Complex(-2.0, 2.0), Complex(2.0, -2.0), 100, 100),
        TestCase(Complex(-2.0, 2.0), Complex(2.0, -2.0), 25, 25),
        TestCase(Complex(-2.0, 2.0), Complex(2.0, -2.0), 1100, 1100),
        TestCase(Complex(-2.0, 2.0), Complex(2.0, -2.0), 1, 1),
        TestCase(Complex(-5.0, 5.0), Complex(5.0, -5.0), 100, 100),
        TestCase(Complex(-5.0, 5.0), Complex(5.0, -5.0), 55, 55),
        TestCase(Complex(-5.0, 5.0), Complex(5.0, -5.0), 1100, 1100),
        TestCase(Complex(-5.0, 5.0), Complex(5.0, -5.0), 1, 1),
        TestCase(Complex(-4.0, 2.0), Complex(4.0, -2.0), 200, 100),
        TestCase(Complex(-4.0, 2.0), Complex(4.0, -2.0), 2, 1),
        TestCase(Complex(-4.0, 2.0), Complex(4.0, -2.0), 2000, 1000)
    )

    @Test
    fun edgeComplexTest() {
        tests.forEach { testCase ->
            val plane = CartesianPlaneConverter(testCase.upLeft, testCase.downRight, testCase.width, testCase.height)

            var result = plane.toComplex(0, 0)
            var expResult = testCase.upLeft.copy()
            assertTrue(result.compareWithPercent(expResult),
                "TEST FAILURE: up left edge! Expected $expResult but was $result")

            result = plane.toComplex(testCase.width, 0)
            expResult = Complex(testCase.downRight.real, testCase.upLeft.imag)
            assertTrue(result.compareWithPercent(expResult),
                "TEST FAILURE: up right edge! Expected $expResult but was $result")

            result = plane.toComplex(testCase.width, testCase.height)
            expResult = testCase.downRight.copy()
            assertTrue(result.compareWithPercent(expResult),
                "TEST FAILURE: down right edge! Expected $expResult but was $result")

            result = plane.toComplex(0, testCase.height)
            expResult = Complex(testCase.upLeft.real, testCase.downRight.imag)
            assertTrue(result.compareWithPercent(expResult),
                "TEST FAILURE: down right edge! Expected $expResult but was $result")
        }
    }

    @Test
    fun edgePixelTest() {
        tests.forEach { testCase ->
            val plane = CartesianPlaneConverter(testCase.upLeft, testCase.downRight, testCase.width, testCase.height)

            var result = plane.toPixel(testCase.upLeft.copy())
            var expResult = Pixel(0, 0)
            assertEquals(result, expResult,
                "TEST FAILURE: up left edge! Expected $expResult but was $result")

            result = plane.toPixel(testCase.downRight.real, testCase.upLeft.imag)
            expResult = Pixel(testCase.width, 0)
            assertEquals(result, expResult,
                "TEST FAILURE: up right edge! Expected $expResult but was $result")

            result = plane.toPixel(testCase.downRight.copy())
            expResult = Pixel(testCase.width, testCase.height)
            assertEquals(result, expResult,
                "TEST FAILURE: down right edge! Expected $expResult but was $result")

            result = plane.toPixel(testCase.upLeft.real, testCase.downRight.imag)
            expResult = Pixel(0, testCase.height)
            assertEquals(result, expResult,
                "TEST FAILURE: down right edge! Expected $expResult but was $result")
        }
    }

}
