/*
 * Copyright (C) 2020. Deglans Dalpasso <deglans@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package io.gitlab.deglans.mandala.utils

import kotlin.test.Ignore
import kotlin.test.Test
import kotlin.test.assertEquals

class ColorPaletteTests {

    @Test
    fun interpolateColorTest() {
        val i = interpolateColor(
            Color(255, 255, 255),
            Color(0, 0, 0),
            0.5
        )
        assertEquals(Color(128, 128, 128), i,
            "TEST FAILURE: ${Color(128, 128, 128)} != $i")
    }

    @Test
    fun interpolateDoubleTest() {
        val i = interpolateDouble(1.0, 0.0, 0.5)
        assertEquals(0.5, i,
            "TEST FAILURE: 0.5 != $i")
    }

    @Test
    fun simplePaletteTest() {
        val colorPalette = ColorPalette(
            3,
            Color(0, 0, 0),
            arrayOf(
                ColorPalette.ColorStop(0.0, Color(255, 0, 0)),
                ColorPalette.ColorStop(0.5, Color(0, 255, 0)),
                ColorPalette.ColorStop(1.0, Color(0, 0, 255))
            )
        )

        assertEquals(Color(255, 0, 0), colorPalette[0])
        assertEquals(Color(0, 255, 0), colorPalette[1])
        assertEquals(Color(0, 0, 255), colorPalette[2])
        assertEquals(Color(0, 0, 0), colorPalette[3])
    }

}
