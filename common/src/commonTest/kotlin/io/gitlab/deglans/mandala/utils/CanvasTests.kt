/*
 * Copyright (C) 2019. Deglans Dalpasso <deglans@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package io.gitlab.deglans.mandala.utils

import kotlin.test.Test
import kotlin.test.assertEquals

class CanvasTests {

    @Test
    fun constructorTest() {
        val canvas = Canvas(100, 100)
        for (x in 0 until canvas.width) {
            for (y in 0 until canvas.height) {
                assertEquals(canvas[x, y], Color(0, 255, 0),
                    "TEST FAILURE: canvas[$x, $y] is not GREEN.")
            }
        }
    }

    @Test
    fun getAndSetTest() {
        val canvas = Canvas(100, 100)
        for (x in 0 until canvas.width) {
            for (y in 0 until canvas.height) {
                canvas[x, y] = Color(255, 0, 0)
            }
        }
        for (x in 0 until canvas.width) {
            for (y in 0 until canvas.height) {
                assertEquals(canvas[x, y], Color(255, 0, 0),
                    "TEST FAILURE: canvas[$x, $y] is not RED.")
            }
        }
    }

}
