/*
 * Copyright (C) 2020. Deglans Dalpasso <deglans@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package io.gitlab.deglans.mandala.utils

import kotlinx.serialization.json.Json
import kotlinx.serialization.json.JsonConfiguration
import kotlin.test.Test
import kotlin.test.assertEquals

class ColorTests {

    class BoundsTests {

        data class TestCase(val actual: Color, val expected: Color)

        private val boundsTests = arrayListOf(
            TestCase(Color(100, 1569, 12), Color(100, 255, 12)),
            TestCase(Color(100, 255, 12), Color(100, 255, 12)),
            TestCase(Color(-3, 1569, 12), Color(0, 255, 12)),
            TestCase(Color(100, 1569, 0), Color(100, 255, 0))
        )

        @Test
        fun boundsTest() {
            boundsTests.forEach {
                assertEquals(it.expected, it.actual,
                    "TEST FAILURE: actual=${it.actual} is different from expected=${it.expected}")
            }
        }

    }

    class SerializableTests {

        data class TestCase(val color: Color, val string: String)

        private val serializableTests = arrayListOf(
            TestCase(Color(100, 15, 12), """{"red":100,"green":15,"blue":12}"""),
            TestCase(Color(100, 255, 12), """{"red":100,"green":255,"blue":12}"""),
            TestCase(Color(0, 9, 12), """{"red":0,"green":9,"blue":12}"""),
            TestCase(Color(100, 5, 0), """{"red":100,"green":5,"blue":0}""")
        )

        @Test
        fun serializableTest() {
            val json = Json(JsonConfiguration.Stable)
            serializableTests.forEach {
                val actual = json.stringify(Color.serializer(), it.color)
                assertEquals(it.string, actual,
                    "TEST FAILURE: actual=${actual} is different from expected=${it.string}")
            }
            serializableTests.forEach {
                val actual = json.parse(Color.serializer(), it.string)
                assertEquals(it.color, actual,
                    "TEST FAILURE: actual=${actual} is different from expected=${it.color}")
            }
        }

    }

}
