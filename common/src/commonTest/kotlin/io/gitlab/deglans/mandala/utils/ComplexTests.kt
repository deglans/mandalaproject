/*
 * Copyright (c) 2019. Deglans Dalpasso <deglans@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package io.gitlab.deglans.mandala.utils

import kotlinx.serialization.json.Json
import kotlinx.serialization.json.JsonConfiguration
import kotlin.Double.Companion.NEGATIVE_INFINITY
import kotlin.Double.Companion.NaN
import kotlin.Double.Companion.POSITIVE_INFINITY
import kotlin.math.nextDown
import kotlin.test.*

class ComplexTests {

    class InterpolateTests {
        
        data class TestCase(val op1: Complex, val op2: Complex, val t: Double, val expectedResult: Complex)

        private val interpolateTests = arrayListOf(
            TestCase(Complex(0.0, 0.0), Complex(2.0, 0.0), 0.5, Complex(1.0, 0.0)),
            TestCase(Complex(0.0, 0.0), Complex(0.0, 2.0), 0.5, Complex(0.0, 1.0)),
            TestCase(Complex(0.0, 0.0), Complex(2.0, 2.0), 0.5, Complex(1.0, 1.0))
        )

        @Test
        fun interpolateTest() {
            interpolateTests.forEach {
                val result = it.op1.interpolate(it.op2, it.t)
                assertTrue(it.expectedResult.compareWithPercent(result),
                    "TEST FAILURE: op1 = ${it.op1} op2 = ${it.op2} t = ${it.t} expectedResult = ${it.expectedResult} result = $result")
            }
        }
        
    }
    
    class BaseOperationTests {
        
        data class TestCase(val op1: Complex, val op2: Complex, val expectedResult: Complex)
        
        private val plusTests = arrayListOf(
            TestCase(Complex(6.80375, -2.11234), Complex(5.66198, 5.9688), Complex(12.4657, 3.85646)),
            TestCase(Complex(8.23295, -6.04897), Complex(-3.29554, 5.36459), Complex(4.9374, -0.684381)),
            TestCase(Complex(-4.44451, 1.0794), Complex(-0.452059, 2.57742), Complex(-4.89656, 3.65682)),
            TestCase(Complex(-2.70431, 0.268018), Complex(9.04459, 8.3239), Complex(6.34028, 8.59192)),
            TestCase(Complex(2.71423, 4.34594), Complex(-7.16795, 2.13938), Complex(-4.45371, 6.48532)),
            TestCase(Complex(-9.67399, -5.14226), Complex(-7.25537, 6.08354), Complex(-16.9294, 0.94127)),
            TestCase(Complex(-6.86642, -1.98111), Complex(-7.40419, -7.82382), Complex(-14.2706, -9.80494)),
            TestCase(Complex(9.97849, -5.63486), Complex(0.258648, 6.78224), Complex(10.2371, 1.14738)),
            TestCase(Complex(2.2528, -4.07937), Complex(2.75105, 0.485744), Complex(5.00384, -3.59362)),
            TestCase(Complex(-0.12834, 9.4555), Complex(-4.14966, 5.42715), Complex(-4.278, 14.8827))
        )
        
        @Test
        fun plusTest() {
            plusTests.forEach {
                var result = it.op1.plus(it.op2)
                assertTrue(it.expectedResult.compareWithPercent(result),
                    "TEST FAILURE: op1 = ${it.op1} op2 = ${it.op2} expectedResult = ${it.expectedResult} result = $result")
                
                result = it.op1 + it.op2
                assertTrue(it.expectedResult.compareWithPercent(result),
                    "TEST FAILURE: op1 = ${it.op1} op2 = ${it.op2} expectedResult = ${it.expectedResult} result = $result")
            }
        }
        
        private val minusTests = arrayListOf(
            TestCase(Complex(0.5349, 5.39828), Complex(-1.99543, 7.83059), Complex(2.53033, -2.43231)),
            TestCase(Complex(-4.33371, -2.95083), Complex(6.15449, 8.38053), Complex(-10.4882, -11.3314)),
            TestCase(Complex(-8.60489, 8.98654), Complex(0.519907, -8.27888), Complex(-9.1248, 17.2654)),
            TestCase(Complex(-6.15572, 3.26454), Complex(7.80465, -3.02214), Complex(-13.9604, 6.28668)),
            TestCase(Complex(-8.71657, -9.59954), Complex(-0.845965, -8.73808), Complex(-7.87061, -0.861456)),
            TestCase(Complex(-5.2344, 9.41268), Complex(8.04416, 7.0184), Complex(-13.2786, 2.39429)),
            TestCase(Complex(-4.66669, 0.795207), Complex(-2.49586, 5.20497), Complex(-2.17082, -4.40977)),
            TestCase(Complex(0.250707, 3.35448), Complex(0.632129, -9.21439), Complex(-0.381421, 12.5689)),
            TestCase(Complex(-1.24725, 8.6367), Complex(8.6162, 4.41905), Complex(-9.86344, 4.21765)),
            TestCase(Complex(-4.31413, 4.77069), Complex(2.79958, -2.91903), Complex(-7.11371, 7.68971))
        )

        @Test
        fun minusTest() {
            minusTests.forEach {
                var result = it.op1.minus(it.op2)
                assertTrue(it.expectedResult.compareWithPercent(result),
                    "TEST FAILURE: op1 = ${it.op1} op2 = ${it.op2} expectedResult = ${it.expectedResult} result = $result")
                
                result = it.op1 - it.op2
                assertTrue(it.expectedResult.compareWithPercent(result),
                    "TEST FAILURE: op1 = ${it.op1} op2 = ${it.op2} expectedResult = ${it.expectedResult} result = $result")
            }
        }

        private val divTests = arrayListOf(
            TestCase(Complex(-7.93658, -7.47849), Complex(-0.0911187, 5.2095), Complex(-1.40847, 1.54812)),
            TestCase(Complex(9.69503, 8.70008), Complex(3.6889, -2.33623), Complex(0.809746, 2.87127)),
            TestCase(Complex(4.99542, -2.62673), Complex(-4.11679, -5.35477), Complex(-0.142467, 0.823361)),
            TestCase(Complex(1.68977, -5.11175), Complex(-6.9522, 4.64297), Complex(-0.507671, 0.396226)),
            TestCase(Complex(-7.4905, 5.86941), Complex(-6.71796, 4.90143), Complex(1.14365, -0.0392784)),
            TestCase(Complex(-8.5094, 9.00208), Complex(-8.94941, 0.431268), Complex(0.996991, -0.95784)),
            TestCase(Complex(-6.47579, -5.19875), Complex(5.95596, 4.65309), Complex(-1.09865, -0.0145499)),
            TestCase(Complex(3.13127, 9.3481), Complex(2.78917, 5.1947), Complex(1.64807, 0.282111)),
            TestCase(Complex(-8.13039, -7.30195), Complex(0.404201, -8.43536), Complex(0.817574, -1.00302)),
            TestCase(Complex(-8.60187, -5.9069), Complex(-0.771591, 6.39355), Complex(-0.750586, 1.43598)),
            TestCase(Complex(0.0, 0.0), Complex(-0.771591, 6.39355), Complex(0.0, 0.0)),
            TestCase(Complex(5.0, 0.0), Complex(1.0, 0.0), Complex(5.0, 0.0))            
        )

        @Test
        fun divTest() {
            divTests.forEach {
                var result = it.op1.div(it.op2)
                assertTrue(it.expectedResult.compareWithPercent(result),
                    "TEST FAILURE: op1 = ${it.op1} op2 = ${it.op2} expectedResult = ${it.expectedResult} result = $result")

                result = it.op1 / it.op2
                assertTrue(it.expectedResult.compareWithPercent(result),
                    "TEST FAILURE: op1 = ${it.op1} op2 = ${it.op2} expectedResult = ${it.expectedResult} result = $result")
            }
        }
        
        private val timesTests = arrayListOf(
            TestCase(Complex(3.75723, -6.68052), Complex(-1.19791, 7.6015), Complex(46.2812, 36.5632)),
            TestCase(Complex(6.58402, -3.39326), Complex(-5.42064, 7.86745), Complex(-8.99331, 70.1931)),
            TestCase(Complex(-2.9928, 3.7334), Complex(9.12937, 1.7728), Complex(-33.9409, 28.7779)),
            TestCase(Complex(3.14608, 7.17353), Complex(-1.2088, 8.4794), Complex(-64.6302, 18.0055)),
            TestCase(Complex(-2.03127, 6.29534), Complex(3.68437, 8.21944), Complex(-59.2281, 6.49848)),
            TestCase(Complex(-0.350187, -5.6835), Complex(9.00505, 8.40257), Complex(44.6025, -54.1227)),
            TestCase(Complex(-7.0468, 7.62124), Complex(2.82161, -1.36093), Complex(-9.51134, 31.0944)),
            TestCase(Complex(2.39193, -4.37881), Complex(5.72004, -3.85084), Complex(-3.18018, -34.2579)),
            TestCase(Complex(-1.05933, -5.47787), Complex(-6.24934, -4.47531), Complex(-17.895, 38.9739)),
            TestCase(Complex(1.12888, -1.66997), Complex(-6.60786, 8.13608), Complex(6.1276, 20.2196))
        )

        @Test
        fun timesTest() {
            timesTests.forEach {
                var result = it.op1.times(it.op2)
                assertTrue(it.expectedResult.compareWithPercent(result),
                    "TEST FAILURE: op1 = ${it.op1} op2 = ${it.op2} expectedResult = ${it.expectedResult} result = $result")

                result = it.op1 * it.op2
                assertTrue(it.expectedResult.compareWithPercent(result),
                    "TEST FAILURE: op1 = ${it.op1} op2 = ${it.op2} expectedResult = ${it.expectedResult} result = $result")
            }
        }
        
        private val powComplexTests = arrayListOf(
            TestCase(Complex(0.0, 1.0), Complex(0.0, 1.0), Complex(0.2078795764, 0.0)),
            TestCase(Complex(2.0, 0.0), Complex(2.0, 0.0), Complex(4.0, 0.0)),
            TestCase(Complex(0.0, 1.0), Complex(2.0, 0.0), Complex(-1.0, 0.0)),
            TestCase(Complex(0.0, 0.0), Complex(2.0, 0.0), Complex(0.0, 0.0)),
            TestCase(Complex(10.0, 0.0), Complex(0.0, 0.0), Complex(1.0, 0.0))
        )

        @Test
        fun powComplexTest() {
            powComplexTests.forEach {
                var result = it.op1.pow(it.op2)
                assertTrue(it.expectedResult.compareWithPercent(result),
                    "TEST FAILURE: op1 = ${it.op1} op2 = ${it.op2} expectedResult = ${it.expectedResult} result = $result")

                result = it.op1 pow it.op2
                assertTrue(it.expectedResult.compareWithPercent(result),
                    "TEST FAILURE: op1 = ${it.op1} op2 = ${it.op2} expectedResult = ${it.expectedResult} result = $result")
            }
        }

        private val logComplexTests = arrayListOf(
            TestCase(Complex(0.0, 1.0), Complex(2.71828, 0.0), Complex(0.0, -0.6366))
        )

        @Test
        fun logComplexTest() {
            logComplexTests.forEach {
                var result = it.op1.log(it.op2)
                assertTrue(it.expectedResult.compareWithPercent(result),
                    "TEST FAILURE: op1 = ${it.op1} op2 = ${it.op2} expectedResult = ${it.expectedResult} result = $result")

                result = it.op1 log it.op2
                assertTrue(it.expectedResult.compareWithPercent(result),
                    "TEST FAILURE: op1 = ${it.op1} op2 = ${it.op2} expectedResult = ${it.expectedResult} result = $result")
            }
        }

    }

    class RandomTests {

        private val upLeft = Complex(-2.0, 2.0)
        private val downRight = Complex(2.0, -2.0)
        private val repetitions = 10

        @Test
        fun randomComplexTest() {
            repeat(repetitions) {
                val z = randomComplex(upLeft, downRight)
                assertTrue(
                    ((z.real.compareTo(upLeft.real) >= 0) && (z.real.compareTo(downRight.real) <= 0) &&
                            (z.imag.compareTo(upLeft.imag) <= 0) && (z.imag.compareTo(downRight.imag) >= 0)),
                    "TEST FAILURE: Complex is not in the range! z = $z upLeft = $upLeft downRight = $downRight")
            }
        }

        @Test
        fun powerIntVersusPowerComplexTest() {
            repeat(repetitions) {
                val base = randomComplex(upLeft, downRight)

                // generate a number between [0.0, 1.0] inclusive
                val f = kotlin.random.Random.nextDouble() / 1.0.nextDown()
                val exp = (-100.0 * (1.0 - f) + 100.0 * f).toInt()

                val powerInt = base.pow(exp)
                val powerComplex = base.pow(Complex(exp.toDouble(), 0.0))

                assertTrue(powerInt.compareWithPercent(powerComplex),
                    "TEST FAILURE: powerInt = $powerInt powerComplex = $powerComplex")
            }
        }

        @Test
        fun sumAndDifferenceTest() {
            repeat(repetitions) {
                // a + b = c
                // a + b - c = 0
                // a - c = 0 - b = diff

                val a = randomComplex(upLeft, downRight)
                val b = randomComplex(upLeft, downRight)
                val c = a.plus(b)
                val zero = a.plus(b).minus(c)
                assertTrue(zero.compareWithPercent(Complex()),
                    "TEST FAILURE: a = $a b = $b c = $c zero = $zero")

                val diff1 = a.minus(c)
                val diff2 = Complex().minus(b)
                assertTrue(diff1.compareWithPercent(diff2),
                    "TEST FAILURE: a = $a b = $b c = $c diff1 = $diff1 diff2 = $diff2")
            }
        }

        @Test
        fun divisionAndMultiplicationTest() {
            repeat(repetitions) {
                // a * b = c
                // (a * b) / c = 1 [if c != 0]
                // b / c = 1 / a

                val a = randomComplexNotZero(upLeft, downRight)
                val b = randomComplexNotZero(upLeft, downRight)
                val c = a.times(b)
                val one = a.times(b).div(c)
                assertTrue(one.compareWithPercent(Complex(1.0, 0.0)),
                    "TEST FAILURE: a = $a b = $b c = $c one = $one")

                val div1 = b.div(c)
                val div2 = Complex(1.0, 0.0).div(a)
                assertTrue(div1.compareWithPercent(div2),
                    "TEST FAILURE: a = $a b = $b c = $c div1 = $div1 div2 = $div2")
            }
        }

        @Ignore // TODO "Math reasons"
        fun powerAndLogarithmTest() {
            repeat(repetitions) {
                // exp(a, b) = c
                // log(a, c) = b

                val a = randomComplex(upLeft, downRight)
                val b = randomComplex(upLeft, downRight)
                val c = a.pow(b)
                val bReturn = a.log(c)
                assertTrue(b.compareWithPercent(bReturn),
                    "TEST FAILURE: a = $a b = $b c = $c bReturn = $bReturn")

                val cReturn = a.pow(bReturn)
                assertTrue(c.compareWithPercent(cReturn),
                    "TEST FAILURE: a = $a b = $b c = $c bReturn = $bReturn cReturn = $cReturn")
            }
        }

        private fun randomComplexNotZero(upLeft: Complex, downRight: Complex): Complex {
            var value: Complex

            do {
                value = randomComplex(upLeft, downRight)
            } while (value.compareWithAbsolute(Complex(), 0.0))

            return value
        }

    }

    class EqualsTests {

        @Test
        fun identityTest() {
            assertEquals(Complex(1.0, 1.0), Complex(1.0, 1.0),
                "TEST FAILURE: two instance of Complex (1.0; 1.0) are not equals!")

            val z = Complex(2.0, 3.0)
            assertEquals(z, z, "TEST FAILURE: an instance of Complex is not equals to itself!")
        }

        @Test
        fun copyTest() {
            val z = Complex(2.0, 3.0)
            assertEquals(z, z.copy(),
                    "TEST FAILURE: an instance of Complex is not equals to it's copy!")
        }

        @Test
        fun infinityTest() {
            assertEquals(Complex(POSITIVE_INFINITY, NEGATIVE_INFINITY), Complex(POSITIVE_INFINITY, NEGATIVE_INFINITY),
                    "TEST FAILURE: Complex (Infinity; -Infinity) is not equals to itself!")
        }

        @Test
        fun zeroTest() {
            assertEquals(Complex(0.0, -0.0), Complex(0.0, -0.0),
                    "TEST FAILURE: Complex (0.0, -0.0) is not equals to itself!")
        }

    }

    class HashTests {

        @Test
        fun simpleHashTest() {
            assertEquals(
                    Complex(42.0, 42.0).hashCode(),
                    Complex(42.0, 42.0).hashCode(),
                    "TEST FAILURE: Complex (42.0; 42.0) hash differ!")
        }

        @Test
        fun infinityHashTest() {
            assertEquals(
                    Complex(POSITIVE_INFINITY, NEGATIVE_INFINITY).hashCode(),
                    Complex(POSITIVE_INFINITY, NEGATIVE_INFINITY).hashCode(),
                    "TEST FAILURE: Complex (Infinity; -Infinity) hash differ!")
        }

        @Test
        fun zeroHashTest() {
            assertEquals(
                    Complex(0.0, -0.0).hashCode(),
                    Complex(0.0, -0.0).hashCode(),
                    "TEST FAILURE: Complex (0.0, -0.0) hash differ!")
        }

    }

    class ParseComplexTests {

        data class TestCase(val s: String, val z: Complex)

        private val parseComplexTests = arrayListOf(
            TestCase("(1; 2)", Complex(1.0, 2.0)),
            TestCase("(1.00 ;1.5)", Complex(1.0, 1.5)),
            TestCase("(1.0;1.5)", Complex(1.0, 1.5)),
            TestCase("(1.00  ;  1.5)", Complex(1.0, 1.5)),
            TestCase("(12.2; 5e9)", Complex(12.2, 5e9)),
            TestCase("(-2E-3; 5e-9)", Complex(-2e-3, 5e-9)),
            TestCase("(-2.2E-3; 5.2e-9)", Complex(-2.2e-3, 5.2e-9)),
            TestCase("(NaN; 25)", Complex(NaN, 25.0)),
            TestCase("(NaN; Infinity)", Complex(NaN, POSITIVE_INFINITY)),
            TestCase("(+0.0; -0.0)", Complex(+0.0, -0.0)),
            TestCase("(Infinity; -Infinity)", Complex(POSITIVE_INFINITY, NEGATIVE_INFINITY)),
            TestCase("(.3; 1)", Complex(0.3, 1.0)) // TODO works???
        )

        @Test
        fun parseComplexTest() {
            //assertDoesNotThrow { parseComplex(value) } TODO is that ok?
            parseComplexTests.forEach {
                assertEquals(parseComplex(it.s), it.z,
                    "TEST FAILURE: String \"${it.s}\" is different from Complex \"${it.z}\"")
            }
        }

        private val parseComplexThrowsTests = arrayListOf(
            "(foo; bar)",
            "(12.2; bar)",
            "(foo; 25)",
            "(NaN; foo)",
            "(-0.l; foo)",
            "(Infinity; foo)",
            "(1k2; foo)" // TODO this should fail!
        )

        @Test
        fun parseComplexThrowsTest() {
            parseComplexThrowsTests.forEach {
                assertFailsWith<NumberFormatException> {
                    parseComplex(it)
                }
            }
        }

    }

    class StringComplexConversionTests {

        data class TestCase(val s: String, val z: Complex)

        private val tests = arrayListOf(
            TestCase("(1.0; 1.0)", Complex(1.0, 1.0)),
            TestCase("(42.0; -42.0)", Complex(42.0, -42.0)),
            TestCase("(-0.002; 5.0E-9)", Complex(-2E-3, 5e-9)),
            TestCase("(NaN; NaN)", Complex(NaN, NaN)),
            TestCase("(Infinity; -Infinity)", Complex(POSITIVE_INFINITY, NEGATIVE_INFINITY)),
            TestCase("(0.0; -0.0)", Complex(0.0, -0.0))
        )

        @Test
        fun toStringTest() {
            tests.forEach {
                assertEquals(it.z.toString(), it.s,
                    "TEST FAILURE: Complex \"${it.z}\" is not equals to String \"${it.s}\"!")
            }
        }

        @Test
        fun parseComplexTest() {
            tests.forEach {
                assertEquals(parseComplex(it.s), it.z,
                    "TEST FAILURE: String \"${it.s}\" is not equals to Complex \"${it.z}\"!")
            }
        }

    }

    class CompareTests {

        data class TestCase(val z: Complex, val value: Complex, val deltaAbs: Double, val deltaPercent: Double)

        private val compareDoesNotThrowsTests = arrayListOf(
            TestCase(Complex(1.0, 1.0), Complex(1.0, 1.0), 0.1, 0.01),
            TestCase(Complex(1.0, 1.0), Complex(POSITIVE_INFINITY, 1.0), POSITIVE_INFINITY, 100.0),
            TestCase(Complex(1.0, 1.0), Complex(1.0, NEGATIVE_INFINITY), POSITIVE_INFINITY, 100.0),
            TestCase(Complex(NaN, 1.0), Complex(NaN, 1.0), 0.1, 0.01),
            TestCase(Complex(1.0, NaN), Complex(1.0, NaN), 0.1, 0.01)
        )

        @Test
        fun compareDoesNotThrowsTest() {
            //assertDoesNotThrow { z.compareWithAbsolute(value, deltaAbs) } TODO is that ok?
            //assertDoesNotThrow { z.compareWithPercent(value, deltaPercent) } TODO is that ok?
            compareDoesNotThrowsTests.forEach {
                it.z.compareWithAbsolute(it.value, it.deltaAbs)
                it.z.compareWithPercent(it.value, it.deltaPercent)
            }
        }
        
        private val compareThrowsTests = arrayListOf(
            TestCase(Complex(1.0, 1.0), Complex(1.0, 1.0), -0.1, -0.01),
            TestCase(Complex(1.0, 1.0), Complex(1.0, 1.0), NaN, NaN),
            TestCase(Complex(1.0, 1.0), Complex(1.0, 1.0), -0.0, -0.0),
            TestCase(Complex(1.0, 1.0), Complex(1.0, 1.0), NEGATIVE_INFINITY, 100.1)
        )

        @Test
        fun compareThrowsTest() {
            compareThrowsTests.forEach {
                assertFailsWith<IllegalArgumentException> {
                    it.z.compareWithAbsolute(it.value, it.deltaAbs)
                    it.z.compareWithPercent(it.value, it.deltaPercent)
                }
            }
        }

        private val compareTrueTests = arrayListOf(
            TestCase(Complex(1.0, 1.0), Complex(1.0, 1.0), 0.1, 0.01),
            TestCase(Complex(1.0, 1.0), Complex(1.0, 1.0), 0.0, 0.0),
            TestCase(Complex(1.0, 1.0), Complex(POSITIVE_INFINITY, 1.0), POSITIVE_INFINITY, 100.0),
            TestCase(Complex(NaN, 1.0), Complex(NaN, 1.0), 0.1, 0.01),
            TestCase(Complex(-1.0, 0.0), Complex(-1.0, 1e-7), 1e-6, 1e-6),
            TestCase(Complex(1.0, 1.0), Complex(0.0, NEGATIVE_INFINITY), POSITIVE_INFINITY, 100.0),
            TestCase(Complex(0.0, NaN), Complex(0.0, NaN), 0.01, 0.01),
            TestCase(Complex(0.0, 0.0), Complex(0.0, 0.0), 0.01, 0.01),
            TestCase(Complex(0.0, 0.0), Complex(0.0, 0.0), 0.0, 0.0)
        )
        
        @Test
        fun compareTrueTest() {
            compareTrueTests.forEach {
                assertTrue(it.z.compareWithAbsolute(it.value, it.deltaAbs),
                    "TEST FAILURE: z = ${it.z} value = ${it.value} deltaAbs = ${it.deltaAbs}")
                assertTrue(it.z.compareWithPercent(it.value, it.deltaPercent),
                    "TEST FAILURE: z = ${it.z} value = ${it.value} deltaAbs = ${it.deltaAbs}")
            }
        }

        // TODO: add some other tests
        private val compareFalseTests = arrayListOf(
            TestCase(Complex(1.0, NaN), Complex(NaN, 1.0), 0.1, 0.01)
        )

        @Test
        fun compareFalseTest() {
            compareFalseTests.forEach {
                assertFalse(it.z.compareWithAbsolute(it.value, it.deltaAbs),
                    "TEST FAILURE: z = ${it.z} value = ${it.value} deltaAbs = ${it.deltaAbs}")
                assertFalse(it.z.compareWithPercent(it.value, it.deltaPercent),
                    "TEST FAILURE: z = ${it.z} value = ${it.value} deltaAbs = ${it.deltaAbs}")
            }
        }

    }

    class SerializableTests {

        data class TestCase(val z: Complex, val string: String)

        private val serializableTests = arrayListOf(
            // TODO 'NaN' is not a valid 'double' as per JSON specification.
            //  You can enable 'serializeSpecialFloatingPointValues' property to serialize such values
            //TestCase(Complex(NaN, NaN), """{"real":NaN,"imag":NaN}"""),
            // TODO 'Infinity' is not a valid 'double' as per JSON specification.
            //  You can enable 'serializeSpecialFloatingPointValues' property to serialize such values
            //TestCase(Complex(NEGATIVE_INFINITY, POSITIVE_INFINITY), """{"real":-Infinity,"imag":Infinity}"""),
            TestCase(Complex(0.0, -0.0), """{"real":0.0,"imag":-0.0}"""),
            TestCase(Complex(25.236, 159.357), """{"real":25.236,"imag":159.357}""")
        )

        @Test
        fun serializableTest() {
            val json = Json(JsonConfiguration.Stable)
            serializableTests.forEach {
                val actual = json.stringify(Complex.serializer(), it.z)
                assertEquals(it.string, actual,
                    "TEST FAILURE: actual=${actual} is different from expected=${it.string}")
            }
            serializableTests.forEach {
                val actual = json.parse(Complex.serializer(), it.string)
                assertEquals(it.z, actual,
                    "TEST FAILURE: actual=${actual} is different from expected=${it.z}")
            }
        }

    }

}
