/*
 * Copyright (C) 2019. Deglans Dalpasso <deglans@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package io.gitlab.deglans.mandala.utils

expect class Image

expect fun Canvas.convert(): Image

expect fun convertColor(c: Color): Int

data class Canvas(
    val width: Int = 500,
    val height: Int = 500
) {

    val canvas = Array(width) {
        Array(height) {
            Color(0, 255, 0)
        }
    }

    operator fun get(x: Int, y: Int): Color {
        return canvas[x][y]
    }

    operator fun set(x: Int, y: Int, color: Color) {
        canvas[x][y] = color
    }

    operator fun set(p: Pixel, color: Color) {
        set(p.x, p.y, color)
    }

}
