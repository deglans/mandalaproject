/*
 * Copyright (c) 2019. Deglans Dalpasso <deglans@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package io.gitlab.deglans.mandala.utils

import kotlin.math.*
import kotlin.random.Random
import kotlinx.serialization.*

/**
 * Create a random [io.gitlab.deglans.mandala.utils.Complex] number between upLeft and downRight.
 *
 * The two number upLeft and downRight define a [io.gitlab.deglans.mandala.utils.Complex] plane within the random number will be generated.
 *
 * @see [Math.random()](https://docs.oracle.com/en/java/javase/11/docs/api/java.base/java/lang/Math.html#random())
 * @param upLeft the upper left point of the plane.
 * @param downRight the downer right point of the plane.
 * @return a random [io.gitlab.deglans.mandala.utils.Complex]
 */
fun randomComplex(upLeft: Complex, downRight: Complex): Complex {
    // generate two number between [0.0, 1.0] inclusive
    val fRe = Random.nextDouble() / 1.0.nextDown()
    val fIm = Random.nextDouble() / 1.0.nextDown()

    val newRe = upLeft.real * (1.0 - fRe) + downRight.real * fRe
    val newIm = downRight.imag * (1.0 - fIm) + upLeft.imag * fIm

    return Complex(newRe, newIm)
}

/**
 * Create a new [io.gitlab.deglans.mandala.utils.Complex] from a String.
 *
 * @param value a string in the format "(real; imag)".
 * @return a [io.gitlab.deglans.mandala.utils.Complex]
 */
fun parseComplex(value: String): Complex {
    val regexDouble = "((([-+])?\\d+\\.?\\d*)(([eE])(([-+])?\\d+\\.?\\d*))?)|((([-+])?\\.\\d*)(([eE])(([-+])?\\d+\\.?\\d*))?)|(Infinity)|(-Infinity)|(NaN)"

    val regexComplex = "^(\\(($regexDouble)\\s*;\\s*($regexDouble)\\))$"

    if (! Regex(regexComplex).matches(value)) {
        throw NumberFormatException("The input string is not in the properly format [value = \"$value\"].")
    }

    val regex = Regex(regexDouble)
    val match = regex.find(value)

    val re: Double
    val im: Double

    if ((match != null) && (match.value.toDoubleOrNull() != null)) {
        re = match.value.toDouble()
    } else {
        throw NumberFormatException("The input string is not in the properly format (real part) [value = \"$value\"].")
    }

    val nextMatch = match.next()

    if ((nextMatch != null) && (nextMatch.value.toDoubleOrNull() != null)) {
        im = nextMatch.value.toDouble()
    } else {
        throw NumberFormatException("The input string is not in the properly format (imaginary part) [value = \"$value\"].")
    }

    return Complex(re, im)
}

/**
 * Complex implements a complex number.
 *
 * Defines complex arithmetic and mathematical function.
 *
 * @see [Complex number on wikipedia](https://en.wikipedia.org/wiki/Complex_number)
 *
 * @author Deglans Dalpasso
 * @version 1.0
 */
@Serializable
data class Complex(val real: Double = 0.0, val imag: Double = 0.0) {

    constructor(value: Complex) : this(value.real, value.imag)

    constructor(value: String) : this(parseComplex(value))

    /**
     * Check if two Complex are equal to within a positive delta.
     *
     * Note: NaN is equal itself.
     *
     * @see [Java 11 Double.compareTo()](https://docs.oracle.com/en/java/javase/11/docs/api/java.base/java/lang/Double.html#compareTo(java.lang.Double))
     *
     * @param value     the Complex number to check.
     * @param delta the maximum delta between this Complex number and value.
     * @return true if the difference between numbers are less than delta; false otherwise.
     */
    fun compareWithAbsolute(value: Complex, delta: Double): Boolean {
        if (delta.compareTo(Double.NaN) == 0 || delta.compareTo(0.0) < 0) {
            throw IllegalArgumentException("delta must be positive.")
        }

        val deltaRe: Double
        val deltaIm: Double

        if (real.compareTo(Double.NaN) == 0 && value.real.compareTo(Double.NaN) == 0) {
            deltaRe = 0.0
        } else if (Double.NaN.compareTo(abs(real - value.real)) == 0) {
            deltaRe = Double.POSITIVE_INFINITY
        } else {
            deltaRe = abs(real - value.real)
        }

        if (imag.compareTo(Double.NaN) == 0 && value.imag.compareTo(Double.NaN) == 0) {
            deltaIm = 0.0
        } else if (Double.NaN.compareTo(abs(imag - value.imag)) == 0) {
            deltaIm = Double.POSITIVE_INFINITY
        } else {
            deltaIm = abs(imag - value.imag)
        }

        // real or imag can be NaN so it is better compareTo function
        return deltaRe.compareTo(delta) <= 0 && deltaIm.compareTo(delta) <= 0 // TODO: check the NaN
    }

    /**
     * Check if two Complex are equal to within a positive delta.
     *
     * Note: NaN is equal itself.
     *
     * @see [Java 11 Double.compareTo()](https://docs.oracle.com/en/java/javase/11/docs/api/java.base/java/lang/Double.html#compareTo(java.lang.Double))
     *
     * @param value     the Complex number to check.
     * @param delta the maximum delta between this Complex number and value.
     * @return true if the difference between numbers are less than delta; false otherwise.
     */
    fun compareWithPercent(value: Complex, delta: Double = 0.01): Boolean {
        if (delta.compareTo(Double.NaN) == 0 || delta.compareTo(0.0) < 0 || delta.compareTo(100.0) > 0) {
            throw IllegalArgumentException("delta must be positive.")
        }

        var cRe = real
        var cIm = imag
        var vRe = value.real
        var vIm = value.imag

        if (real.compareTo(0.0) == 0 || real.compareTo(-0.0) == 0) {
            cRe = Double.MIN_VALUE
        }

        if (imag.compareTo(0.0) == 0 || imag.compareTo(-0.0) == 0) {
            cIm = Double.MIN_VALUE
        }

        if (value.real.compareTo(0.0) == 0 || value.real.compareTo(-0.0) == 0) {
            vRe = Double.MIN_VALUE
        }

        if (value.imag.compareTo(0.0) == 0 || value.imag.compareTo(-0.0) == 0) {
            vIm = Double.MIN_VALUE
        }

        var deltaRe: Double
        var deltaIm: Double

        if (cRe.compareTo(Double.NaN) == 0 && vRe.compareTo(Double.NaN) == 0) {
            deltaRe = 0.0
        } else if (Double.NaN.compareTo(abs(cRe / vRe) - 1.0) == 0) {
            deltaRe = 100.0
        } else {
            deltaRe = abs(cRe / vRe) - 1.0
        }

        if (cIm.compareTo(Double.NaN) == 0 && vIm.compareTo(Double.NaN) == 0) {
            deltaIm = 0.0
        } else if (Double.NaN.compareTo(abs(cIm / vIm) - 1.0) == 0) {
            deltaIm = 100.0
        } else {
            deltaIm = abs(cIm / vIm) - 1.0
        }

        if (deltaRe.compareTo(Double.POSITIVE_INFINITY) == 0) {
            deltaRe = 100.0
        }

        if (deltaIm.compareTo(Double.POSITIVE_INFINITY) == 0) {
            deltaIm = 100.0
        }

        // real or imag can be NaN so it is better compareTo function
        return deltaRe.compareTo(delta) <= 0 && deltaIm.compareTo(delta) <= 0 // TODO: check the NaN
    }

    override fun equals(other: Any?): Boolean {
        if (this === other) return true

        other as Complex

        // TODO: check the NaN, Infinity and -0.0 https://docs.oracle.com/en/java/javase/11/docs/api/java.base/java/lang/Double.html#compareTo(java.lang.Double)
        if (real.compareTo(other.real) != 0) return false
        if (imag.compareTo(other.imag) != 0) return false
//        if (compareWithAbsolute(other, 0.0)) return false

        return true
    }

    override fun hashCode(): Int {
        var result = real.hashCode()
        result = 31 * result + imag.hashCode()
        return result
    }

    override fun toString(): String {
        return "($real; $imag)"
    }

    /**
     * Sum of Complex number.
     *
     * (a+i*b) + (c+i*d) = (a+c) + i*(b+d)
     *
     * @param value is the number to add.
     * @return this plus value.
     */
    operator fun plus(value: Complex): Complex {
        return Complex(real + value.real, imag + value.imag)
    }

    /**
     * Difference of Complex number.
     *
     * (a+i*b) - (c+i*d) = (a-c) + i*(b-d)
     *
     * @param value is the number to subtract.
     * @return this minus value.
     */
    operator fun minus(value: Complex): Complex {
        return Complex(real - value.real, imag - value.imag)
    }

    /**
     * Multiplication of Complex number.
     *
     * (a+i*b) * (c+i*d) = (a*c - b*d) + i*(b*c + a*d)
     *
     * @param value is the number to multiply by.
     * @return this multiplied by value.
     */
    operator fun times(value: Complex): Complex {
        val newRe = real * value.real - imag * value.imag
        val newIm = imag * value.real + real * value.imag

        return Complex(newRe, newIm)
    }

    /**
     * Division of Complex number.
     *
     * (a+i*b) / (c+i*d) = (a*c+b*d)/den + i*((b*c-a*d)/den)
     * where den = c*c+d*d
     *
     * If den is zero the result is infinity or NaN as:
     *
     *  * 1/0 yields Infinity.
     *  * (-1)/0 yields -Infinity.
     *  * 0/0 yields NaN.
     *
     * These "numbers" are properly defined in IEEE 754.
     *
     * @param value is the number to divide by.
     * @return this divided by value.
     */
    operator fun div(value: Complex): Complex {
        val den = value.real * value.real + value.imag * value.imag

        val newRe = (real * value.real + imag * value.imag) / den
        val newIm = (imag * value.real - real * value.imag) / den

        return Complex(newRe, newIm)
    }

    /**
     * Returns the modulus of this Complex number.
     *
     * The modulus is defined as:
     * |a+i*b| = √(a*a + b*b)
     *
     * @return the modulus of this Complex number.
     */
    fun mod(): Double {
        return sqrt(real * real + imag * imag)
    }

    /**
     * Returns the semi-modulus of this Complex number.
     *
     * The semi-modulus is defined as:
     * |a+i*b|² = a*a + b*b
     *
     * @return the semi-modulus of this Complex number.
     */
    fun semiMod(): Double {
        return real * real + imag * imag
    }

    /**
     * Returns the argument in radians of this Complex number.
     *
     * @return the argument in radians of this Complex number.
     */
    fun arg(): Double {
        return atan2(imag, real)
    }

    /**
     * Returns this Complex number raised to the power of value using De Moivre's formula.
     *
     * @param value the exponent.
     * @return this Complex number raised to the power of value.
     */
    infix fun pow(value: Int): Complex {
        val newMod = mod().pow(value.toDouble())
        val newArg = arg() * value.toDouble()

        return Complex(newMod * cos(newArg), newMod * sin(newArg))
    }

    /**
     * Returns this Complex number raised to the power of another Complex number.
     *
     * @see [Complex exponentiation on wolfram](http://mathworld.wolfram.com/ComplexExponentiation.html)
     *
     * @param value the exponent.
     * @return this Complex number raised to the power of value.
     */
    infix fun pow(value: Complex): Complex {
        val newMod: Double
        val newArg: Double
        val newRe: Double
        val newIm: Double

        // workaround: (new Complex(0.0d, 0.0d).pow(anotherComplexNumber)) == 0 even!
        if (real.compareTo(0.0) == 0 && imag.compareTo(0.0) == 0) {
            newRe = 0.0
            newIm = 0.0
        } else {
            newMod = semiMod().pow(value.real / 2) * exp(-value.imag * arg())
            newArg = value.real * arg() + 0.5 * value.imag * ln(semiMod())

            newRe = newMod * cos(newArg)
            newIm = newMod * sin(newArg)
        }

        return Complex(newRe, newIm)
    }

    /**
     * Use this Complex number as a base for Complex logarithm.
     *
     * @see [Complex logarithm](https://en.wikipedia.org/wiki/Complex_logarithm)
     *
     * @param value the value to calculate.
     * @return the logarithm.
     */
    infix fun log(value: Complex): Complex {
        val lnArg = Complex(ln(value.mod()), value.arg())
        val lnBase = Complex(ln(mod()), arg())

        return lnArg.div(lnBase)
    }

    /**
     * The function calculates an interpolated value along the fraction t between 0.0 and 1.0.
     *
     * When t = 1.0 endVal is returned, when t = 0 this is returned.
     *
     * @param endValue target value.
     * @param t fraction between 0.0 and 1.0.
     * @return the interpolated value.
     */
    fun interpolate(endValue: Complex, t: Double): Complex {
        if (t > 1.0 || t < 0.0) {
            throw IllegalArgumentException()
        }

        val w = Complex(t, 0.0)
        val delta = endValue.minus(this).times(w)

        return this.plus(delta)
    }

}
