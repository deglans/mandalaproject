/*
 * Copyright (C) 2020. Deglans Dalpasso <deglans@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package io.gitlab.deglans.mandala.utils

import kotlinx.serialization.*

@Serializable
data class Color(var red: Int, var green: Int, var blue: Int) {

    init {
        when {
            red < 0 -> this.red = 0
            red > 255 -> this.red = 255
            else -> this.red = red
        }
        when {
            green < 0 -> this.green = 0
            green > 255 -> this.green = 255
            else -> this.green = green
        }
        when {
            blue < 0 -> this.blue = 0
            blue > 255 -> this.blue = 255
            else -> this.blue = blue
        }
    }

}
