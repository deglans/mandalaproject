/*
 * Copyright (C) 2020. Deglans Dalpasso <deglans@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package io.gitlab.deglans.mandala.mandelbrot

import io.gitlab.deglans.mandala.utils.*
import kotlinx.serialization.Serializable

const val DEFAULT_DEPTH = 100

class MandelbrotSimple(private val dataBox: DataBox) {

    private val cartesianPlaneConverter = CartesianPlaneConverter(
        dataBox.upLeft,
        dataBox.downRight,
        dataBox.width,
        dataBox.height
    )

    private val canvas = Canvas(
        dataBox.width,
        dataBox.height
    )

    private val colorPalette = ColorPalette(
        dataBox.depth,
        dataBox.color,
        dataBox.colorsStops
    )

    fun calculate(): Canvas {
        // TODO use thread / coroutines
        for (w in 0 until dataBox.width) {
            for (h in 0 until dataBox.height) {
                canvas[w, h] = calculatePoint(w, h)
            }
        }
        return canvas
    }

    private fun calculatePoint(x: Int, y: Int): Color {
        var n = 0
        var z = Complex()
        val c = cartesianPlaneConverter.toComplex(x, y)

        do {
            n++
            z = z.pow(2) + c
        } while ((n < dataBox.depth) && (z.semiMod() < 4.0))

        return colorPalette[n]
    }

    @Serializable
    data class DataBox(
        val upLeft: Complex = DEFAULT_UP_LEFT,
        val downRight: Complex = DEFAULT_DOWN_RIGHT,
        val width: Int = DEFAULT_WIDTH,
        val height: Int = DEFAULT_HEIGHT,
        val depth: Int = DEFAULT_DEPTH,
        val color: Color = DEFAULT_COLOR,
        val colorsStops: Array<ColorPalette.ColorStop> = DEFAULT_COLORS_STOPS
    ) {

        override fun equals(other: Any?): Boolean {
            if (this === other) return true
            if (other == null || this::class != other::class) return false

            other as DataBox

            if (upLeft != other.upLeft) return false
            if (downRight != other.downRight) return false
            if (width != other.width) return false
            if (height != other.height) return false
            if (depth != other.depth) return false
            if (color != other.color) return false
            if (!colorsStops.contentEquals(other.colorsStops)) return false

            return true
        }

        override fun hashCode(): Int {
            var result = upLeft.hashCode()
            result = 31 * result + downRight.hashCode()
            result = 31 * result + width
            result = 31 * result + height
            result = 31 * result + depth
            result = 31 * result + color.hashCode()
            result = 31 * result + colorsStops.contentHashCode()
            return result
        }

    }

}
