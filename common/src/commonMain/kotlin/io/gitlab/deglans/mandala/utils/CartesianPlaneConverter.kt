/*
 * Copyright (C) 2019. Deglans Dalpasso <deglans@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package io.gitlab.deglans.mandala.utils

val DEFAULT_UP_LEFT: Complex
    get() = Complex(-2.0, 2.0)

val DEFAULT_DOWN_RIGHT: Complex
    get() = Complex(2.0, -2.0)

const val DEFAULT_WIDTH = 500

const val DEFAULT_HEIGHT = 500

/**
 * CartesianPlaneConverter is a converter between Complex coordinates and Pixel coordinates.
 *
 * @version 1.0
 * @author Deglans Dalpasso
 */
class CartesianPlaneConverter(
    val upLeft: Complex = DEFAULT_UP_LEFT,
    val downRight: Complex = DEFAULT_DOWN_RIGHT,
    val width: Int = DEFAULT_WIDTH,
    val height: Int = DEFAULT_HEIGHT
) {

    fun getCenter(): Complex =
        Complex(
            upLeft.real + getSideX() / 2,
            upLeft.imag - getSideY() / 2
        )

    fun getSideX(): Double = downRight.real - upLeft.real

    fun getSideY(): Double = upLeft.imag - downRight.imag

    fun getScale(): Double =
        if (width > height) {
            height / getSideY()
        } else {
            width / getSideX()
        }

    fun toComplex(p: Pixel): Complex {
        return toComplex(p.x, p.y)
    }

    fun toComplex(x: Int, y: Int): Complex {
        val pRe = upLeft.real + (x.toDouble() / getScale())
        val pIm = upLeft.imag + (y.toDouble() / - getScale())
        return Complex(pRe, pIm)
    }

    fun toPixel(z: Complex): Pixel {
        return toPixel(z.real, z.imag)
    }

    fun toPixel(re: Double, im: Double): Pixel {
        val px = getScale() * (re - upLeft.real)
        val py = - getScale() * (im - upLeft.imag)
        return Pixel(px.toInt(), py.toInt())
    }

}
