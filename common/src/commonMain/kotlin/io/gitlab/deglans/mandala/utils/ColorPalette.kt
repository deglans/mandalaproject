/*
 * Copyright (C) 2020. Deglans Dalpasso <deglans@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package io.gitlab.deglans.mandala.utils

import io.gitlab.deglans.mandala.utils.ColorPalette.ColorStop
import kotlinx.serialization.Serializable
import kotlin.math.roundToInt

const val DEFAULT_LENGTH: Int = 100

val DEFAULT_COLOR = Color(0, 0, 0)

val DEFAULT_COLORS_STOPS: Array<ColorStop>
    get() = arrayOf(
        ColorStop(0.00, Color(40, 0, 0)),
        ColorStop(0.17, Color(255, 0, 0)),
        ColorStop(0.25, Color(255, 255, 255)),
        ColorStop(0.30, Color(255, 0, 0)),
        ColorStop(0.50, Color(100, 0, 0)),
        ColorStop(0.75, Color(255, 0, 0)),
        ColorStop(1.00, Color(50, 0, 0))
    )

fun interpolateColor(startValue: Color, endValue: Color, t: Double): Color {
    if (t <= 0.0) return startValue
    if (t >= 1.0) return endValue

    return Color(
        (startValue.red + (endValue.red - startValue.red) * t).roundToInt(),
        (startValue.green + (endValue.green - startValue.green) * t).roundToInt(),
        (startValue.blue + (endValue.blue - startValue.blue) * t).roundToInt()
    )
}

fun interpolateDouble(startValue: Double, endValue: Double, t: Double): Double {
    if (t <= 0.0) return startValue
    if (t >= 1.0) return endValue

    return startValue + (endValue - startValue) * t
}

/**
 * ColorPalette contains all the information about how to color fractals.
 *
 * @version 0.1
 * @author Deglans Dalpasso
 */
data class ColorPalette(
    val length: Int = DEFAULT_LENGTH,
    val color: Color = DEFAULT_COLOR,
    val colorsStops: Array<ColorStop> = DEFAULT_COLORS_STOPS
) {

    init {
        if (length <= 0)
            throw IndexOutOfBoundsException("length must be > 0")

        if (colorsStops.size < 2)
            throw IndexOutOfBoundsException("The length of colorsStops must be >= 2")

        for (cs in colorsStops) {
            if (cs.stop > 1.0) throw IndexOutOfBoundsException("All stops value must be <= 1.0")
            if (cs.stop < 0.0) throw IndexOutOfBoundsException("All stops value must be >= 0.0")
        }

        if (colorsStops.first().stop != 0.0) throw IndexOutOfBoundsException("The first stop must be 0.0")
        if (colorsStops.last().stop != 1.0) throw IndexOutOfBoundsException("The last stop must be 1.0")

        var i = 0
        do {
            if (colorsStops[i].stop >= colorsStops[i+1].stop)
                throw IndexOutOfBoundsException("The stop value must be in the right order.")
            i++
        } while (i < colorsStops.size - 1)
    }

    operator fun get(index: Int): Color {
        if (index >= length) return color

        val relative = index.toDouble() / (length - 1)
        var i = 0
        do {
            if ((colorsStops[i].stop <= relative) && (relative <= colorsStops[i+1].stop))
                break
            i++
        } while (i < colorsStops.size - 1)

        val p = (relative - colorsStops[i].stop) / (colorsStops[i+1].stop - colorsStops[i].stop)
        return interpolateColor(colorsStops[i].color, colorsStops[i+1].color, p)
    }

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (other == null || this::class != other::class) return false

        other as ColorPalette

        if (length != other.length) return false
        if (color != other.color) return false
        if (!colorsStops.contentEquals(other.colorsStops)) return false

        return true
    }

    override fun hashCode(): Int {
        var result = length
        result = 31 * result + color.hashCode()
        result = 31 * result + colorsStops.contentHashCode()
        return result
    }

    @Serializable
    data class ColorStop(val stop: Double, val color: Color)

}
