/*
 * Copyright (C) 2019. Deglans Dalpasso <deglans@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import org.jetbrains.kotlin.gradle.tasks.KotlinCompile

plugins {
    id("maven-publish")
    kotlin("multiplatform")
    id("com.android.library")
    kotlin("android.extensions")
    kotlin("plugin.serialization") version "1.3.72"
    //jacoco
}

val nexusUsername: String by project
val nexusPassword: String by project
val nexusUrl: String by project
val isCI: String by project

publishing {
    repositories {
        maven {
            if (isCI == "true") {
                url = uri("/opt/repo")
            } else {
                credentials {
                    username = nexusUsername
                    password = nexusPassword
                }
                url = uri(nexusUrl)
            }
        }
    }
}

android {
    compileSdkVersion(29)

    compileOptions {
        sourceCompatibility = JavaVersion.VERSION_1_8
        targetCompatibility = JavaVersion.VERSION_1_8
    }

    tasks.withType<KotlinCompile> {
        kotlinOptions.jvmTarget = JavaVersion.VERSION_1_8.toString()
    }

    sourceSets {
        getByName("main") {
            manifest.srcFile("src/androidMain/AndroidManifest.xml")
            java.srcDirs("src/androidMain/kotlin")
            res.srcDirs("src/androidMain/res")
            resources.srcDirs("src/androidMain/resources")
        }
        getByName("test") {
            java.srcDirs("src/androidTest/kotlin")
            res.srcDirs("src/androidTest/res")
            resources.srcDirs("src/androidTest/resources")
        }
    }
}

kotlin {
    jvm {
        val main by compilations.getting {
            kotlinOptions {
                jvmTarget = JavaVersion.VERSION_11.toString()
            }
        }

        val compileTestKotlinJvm: KotlinCompile by tasks
        compileTestKotlinJvm.kotlinOptions.jvmTarget = JavaVersion.VERSION_11.toString()

        tasks.withType<Test> {
            useJUnitPlatform()
            testLogging {
                events("passed", "skipped", "failed")
            }
            //ignoreFailures = true
        }
    }

    android {
        publishLibraryVariants("release", "debug")

        tasks.withType<KotlinCompile> {
            kotlinOptions.jvmTarget = JavaVersion.VERSION_1_8.toString()
        }

        tasks.withType<Test> {
            useJUnitPlatform()
            testLogging {
                events("passed", "skipped", "failed")
            }
            //ignoreFailures = true
        }
    }

    sourceSets {
        val commonMain by getting {
            dependencies {
                implementation(kotlin("stdlib-common"))
                implementation("org.jetbrains.kotlinx:kotlinx-serialization-runtime-common:0.20.0")
                //implementation("org.jetbrains.kotlinx:kotlinx-coroutines-core:1.3.5")
            }
        }
        val commonTest by getting {
            dependencies {
                implementation(kotlin("test-common"))
                implementation(kotlin("test-annotations-common"))
            }
        }

        val jvmMain by getting {
            dependencies {
                implementation(kotlin("stdlib-jdk8"))
                implementation("org.jetbrains.kotlinx:kotlinx-serialization-runtime:0.20.0")
            }
        }
        val jvmTest by getting {
            dependencies {
                implementation(kotlin("test"))
                implementation(kotlin("test-junit5"))
                implementation("org.junit.jupiter:junit-jupiter-api:5.6.1")
                implementation("org.junit.jupiter:junit-jupiter-params:5.6.1")
                runtimeOnly("org.junit.jupiter:junit-jupiter-engine:5.6.1")
            }
        }

        val androidMain by getting {
            dependencies {
                implementation(kotlin("stdlib-jdk8"))
                implementation("org.jetbrains.kotlinx:kotlinx-serialization-runtime:0.20.0")
            }
        }
        val androidTest by getting {
            dependencies {
                implementation(kotlin("test"))
                implementation(kotlin("test-junit5"))
                implementation("org.junit.jupiter:junit-jupiter-api:5.6.1")
                implementation("org.junit.jupiter:junit-jupiter-params:5.6.1")
                runtimeOnly("org.junit.jupiter:junit-jupiter-engine:5.6.1")
            }
        }
    }
}


//jacoco {
//    toolVersion = "0.8.5"
//}

//    tasks.withType<JacocoReport> {
//        reports {
//            xml.isEnabled = true
//            csv.isEnabled = false
//            html.isEnabled = true
//        }
//    }

//    tasks.jacocoTestCoverageVerification {
//        violationRules {
//            rule {
//                limit {
//                    minimum = "0.75".toBigDecimal()
//                }
//            }
//        }
//    }


//tasks.jacocoTestReport {
////tasks.withType<JacocoReport> {
//    dependsOn("jvmTest")
//    reports {
//        xml.isEnabled = true
//        csv.isEnabled = false
//        html.isEnabled = true
//    }
//}
