# MandalaProject

## Initialization of the project

Command issued:

~~~
$ gradle init --dsl kotlin --type basic
Starting a Gradle Daemon (subsequent builds will be faster)

Project name (default: MandalaProject):


> Task :init
Get more help with your project: https://guides.gradle.org/creating-new-gradle-builds

BUILD SUCCESSFUL in 19s
2 actionable tasks: 2 executed
$ ls -la
totale 44K
drwxrwxr-x  4 deglans deglans 4,0K dic  8 18:13 .
drwxr-xr-x 25 deglans deglans 4,0K nov 21 20:49 ..
-rw-rw-r--  1 deglans deglans  200 dic  8 18:13 build.gradle.kts
-rw-rw-r--  1 deglans deglans  154 dic  8 18:13 .gitattributes
-rw-rw-r--  1 deglans deglans  103 dic  8 18:13 .gitignore
drwxrwxr-x  4 deglans deglans 4,0K dic  8 18:13 .gradle
drwxrwxr-x  3 deglans deglans 4,0K dic  8 18:13 gradle
-rwxrwxr-x  1 deglans deglans 5,7K dic  8 18:13 gradlew
-rw-rw-r--  1 deglans deglans 2,9K dic  8 18:13 gradlew.bat
-rw-rw-r--  1 deglans deglans  363 dic  8 18:13 settings.gradle.kts
~~~

## Creation of the GPG keyring



## Initialization of the git repository

Commands issued:

~~~
$ git init
Initialised empty Git repository in /documenti/deglans/Programs/MandalaProject/.git/
$ git add --all
$ git status
Sul branch master

No commits yet

Changes to be committed:
  (use "git rm --cached <file>..." to unstage)

        new file:   .gitattributes
        new file:   .gitignore
        new file:   build.gradle.kts
        new file:   gradle/wrapper/gradle-wrapper.jar
        new file:   gradle/wrapper/gradle-wrapper.properties
        new file:   gradlew
        new file:   gradlew.bat
        new file:   settings.gradle.kts

$ git commit -m "init project with \"gradle init --dsl kotlin --type basic\""
[master (root-commit) 0fda530] init project with "gradle init --dsl kotlin --type basic"
 8 files changed, 315 insertions(+)
 create mode 100644 .gitattributes
 create mode 100644 .gitignore
 create mode 100644 build.gradle.kts
 create mode 100644 gradle/wrapper/gradle-wrapper.jar
 create mode 100644 gradle/wrapper/gradle-wrapper.properties
 create mode 100755 gradlew
 create mode 100644 gradlew.bat
 create mode 100644 settings.gradle.kts
$ git remote add origin git@gitlab.com:deglans/mandalaproject.git
$ git push -u origin --all
Enumerating objects: 12, done.
Counting objects: 100% (12/12), done.
Delta compression using up to 4 threads
Compressing objects: 100% (11/11), done.
Writing objects: 100% (12/12), 56.70 KiB | 5.67 MiB/s, done.
Total 12 (delta 0), reused 0 (delta 0)
To gitlab.com:deglans/mandalaproject.git
 * [new branch]      master -> master
Branch 'master' set up to track remote branch 'master' from 'origin'.
$ git push -u origin --tags
Everything up-to-date
~~~

~~~
$ git add --all
$ git commit -m "Added a standard GPL 3.0 license."
[master 956b189] Added a standard GPL 3.0 license.
 1 file changed, 674 insertions(+)
 create mode 100644 LICENSE

~~~

## Docker
~~~
$ docker pull ubuntu:19.10
$ docker run



~~~


