/*
 * Copyright (C) 2020. Deglans Dalpasso <deglans@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package io.gitlab.deglans.mandala.utils

import com.charleskorn.kaml.Yaml
import io.gitlab.deglans.mandala.mandelbrot.MandelbrotSimple
import kotlinx.serialization.Serializable
import java.io.File
import javax.imageio.ImageIO

class MandelbrotFromYamlFile(val path: String) {

    private val yamlDataBox: YamlDataBox

    init {
        yamlDataBox = loadYaml()
    }

    private fun loadYaml(): YamlDataBox {
        val content = File(path).readText()
        return Yaml.default.parse(YamlDataBox.serializer(), content)
    }

    fun execute() {
        val mandelbrot = MandelbrotSimple(yamlDataBox.data)
        val canvas = mandelbrot.calculate()
        val bufferedImage = canvas.convert()
        val file = File(yamlDataBox.path)

        try {
            ImageIO.write(bufferedImage, file.extension, file)
        } catch (e: Exception) {
            println(e.toString())
        }
    }

    @Serializable
    data class YamlDataBox(
        val path: String = "mandelbrot.bmp",
        val data: MandelbrotSimple.DataBox = MandelbrotSimple.DataBox()
    )

}
