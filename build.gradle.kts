/*
 * Copyright (C) 2019. Deglans Dalpasso <deglans@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

// https://github.com/gradle/kotlin-dsl-samples/blob/master/samples/multi-kotlin-project-with-buildSrc/build.gradle.kts
// https://github.com/gradle/kotlin-dsl-samples/tree/master/samples/hello-android
// https://www.bugsnag.com/blog/kotlin-multiplatform
// https://github.com/bugsnag/kotlin-multiplatform-example

plugins {
    base
    //id("org.sonarqube") version "2.8"
}

buildscript {
    repositories {
        google()
        jcenter()
        mavenCentral()
    }
    dependencies {
        classpath("com.android.tools.build:gradle:3.6.2")
        classpath("org.jetbrains.kotlin:kotlin-gradle-plugin:1.3.72")
    }
}

val isCI: String by project
val nexusUrl: String by project

allprojects {
    group = "io.gitlab.deglans.mandala"
    version = "1.0.0-alpha17-SNAPSHOT"

    repositories {
        google()
        jcenter()
        mavenCentral()
        if (isCI == "true") {
            maven("/opt/repo")
        } else {
            maven(nexusUrl)
        }
    }
}

//sonarqube {
//    properties {
//        property("sonar.projectKey", "deglans_mandalaproject")
//        property("sonar.organization", "deglans")
//    }
//}
